import json
import time
from datetime import datetime, timedelta
from multiprocessing import Lock, Process

import zmq
from kinematics import *


class BaseSensor(Process):

    def __init__(self, sub_add, pub_add):
        Process.__init__(self)
        self.subscribes_to = sub_add
        self.publishes_to = pub_add


class Sensor(BaseSensor):

    def __init__(self, sub_add, pub_add):
        BaseSensor.__init__(self, sub_add, pub_add)

        self.acc = [0, 0, 0]
        self.vel = [0, 0]
        self.tilt = [0, 0]
        self.ts = None
        self.device_id = None
        self.battery = None

    def get_telemetry_packet(self):

        telemetryData, accData, velData, tiltData, paramsData = {}, {}, {}, {}, {}

        accData['x'] = self.acc[0]
        accData['y'] = self.acc[1]
        accData['z'] = self.acc[2]

        velData['x'] = self.vel[0]
        velData['y'] = self.vel[1]

        tiltData['Roll'] = self.tilt[0]
        tiltData['Pitch'] = self.tilt[1]

        paramsData['acc'] = accData
        paramsData['vel'] = velData
        paramsData['battery'] = self.battery
        paramsData['tilt'] = tiltData

        telemetryData['params'] = paramsData
        telemetryData['device_id'] = self.device_id
        telemetryData['timestamp'] = self.ts

        return telemetryData

    def run(self):

        ctx = zmq.Context()
        socket = {}

        for line in self.subscribes_to:
            topic, addr = line
            socket[topic] = ctx.socket(zmq.SUB)
            socket[topic].connect(addr)
            socket[topic].setsockopt_string(
                zmq.SUBSCRIBE, unicode(topic))

        for line in self.publishes_to:
            topic, addr = line
            socket[topic] = ctx.socket(zmq.PUB)
            socket[topic].bind(addr)

        while(True):

            for topic, addr in self.subscribes_to:
                decodedData = None

                try:
                    decodedData = socket[topic].recv(flags=zmq.NOBLOCK)
                except zmq.ZMQError:
                    decodedData = None
                    continue

                if decodedData is not None:

                    _, recv_packet_ser = decodedData.split('!')
                    recv_packet_deser = json.loads(recv_packet_ser)

                    timestamp = recv_packet_deser['timestamp']
                    device_id = recv_packet_deser['device_id']
                    battery = recv_packet_deser['params']['battery']

                    self.device_id = device_id
                    self.battery = battery

                    acc_x = recv_packet_deser['params']['acc']['x']
                    acc_y = recv_packet_deser['params']['acc']['y']
                    acc_z = recv_packet_deser['params']['acc']['z']

                    if self.ts is not None:

                        prev_ax = self.acc[0]
                        prev_ay = self.acc[1]
                        prev_az = self.acc[2]

                        dt = (datetime.strptime(timestamp, '%Y-%m-%d %H:%M:%S.%f') -
                              datetime.strptime(self.ts, '%Y-%m-%d %H:%M:%S.%f')).total_seconds()
                        roll, pitch = get_pitch_and_roll(
                            acc_x, acc_y, acc_z)

                        self.tilt[0] = roll
                        self.tilt[1] = pitch

                        self.ts = timestamp
                        self.acc[0] = acc_x
                        self.acc[1] = acc_y
                        self.acc[2] = acc_z

                        dvx = get_change_in_velocity(dt, prev_ax, acc_x)
                        dvy = get_change_in_velocity(dt, prev_ay, acc_y)

                        self.vel[0] += dvx
                        self.vel[1] += dvy

                    else:
                        self.ts = timestamp

                        self.acc[0] = acc_x
                        self.acc[1] = acc_y
                        self.acc[2] = acc_z
                        continue

                    sendData = self.get_telemetry_packet()
                    pub_topic, _ = self.publishes_to[0]

                    try:
                        socket[pub_topic].send("{}!{}".format(
                            pub_topic, json.dumps(sendData)), flags=zmq.NOBLOCK)

                    except zmq.ZMQError:
                        continue

                else:
                    continue
            else:
                continue
