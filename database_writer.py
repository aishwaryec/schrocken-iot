import json
import sqlite3
import time
import uuid
from datetime import datetime, timedelta
from multiprocessing import Lock, Process

import kinematics
import zmq


def save_to_db(conn, uuid, deviceid, timestamp, data):

		try:
				query = '''INSERT INTO telemetry (uuid, deviceid, timestamp, data) VALUES(?,?,?,?)'''
				c = conn.cursor()
				c.execute(query, (uuid, deviceid, timestamp, data))
				conn.commit()

		except Exception as e:
				print('ERROR {}\n'.format(e))


class BaseWriter(Process):

		def __init__(self, sub_add, pub_add):
				Process.__init__(self)
				self.subscribes_to = sub_add
				self.publishes_to = pub_add


class DBWriter(BaseWriter):

		def __init__(self, db, sub_add, pub_add):
				BaseWriter.__init__(self, sub_add, pub_add)
				self.db = db

		def run(self):

				conn = sqlite3.connect(
						self.db, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
				conn.text_factory = str
				c = conn.cursor()
				c.execute('''CREATE TABLE IF NOT EXISTS telemetry (
										uuid BLOB NOT NULL PRIMARY KEY,
										deviceid TEXT NOT NULL,
										timestamp TEXT NOT NULL,
										data BLOB NOT NULL)
									''')
				conn.commit()

				ctx = zmq.Context()
				socket = {}

				for line in self.subscribes_to:
						topic, addr = line
						socket[topic] = ctx.socket(zmq.SUB)
						socket[topic].connect(addr)
						socket[topic].setsockopt_string(
								zmq.SUBSCRIBE, unicode(topic))

				for line in self.publishes_to:
						topic, addr = line
						socket[topic] = ctx.socket(zmq.PUB)
						socket[topic].bind(addr)

				while(True):

						for line in self.subscribes_to:
								topic, addr = line
								decodedData = None

								try:
										decodedData = socket[topic].recv(flags=zmq.NOBLOCK)
								except zmq.ZMQError:

										decodedData = None
										continue

								if decodedData is not None:

										_, recv_packet_ser = decodedData.split('!')
										recv_packet_deser = json.loads(recv_packet_ser)

										timestamp = recv_packet_deser['timestamp']
										device_id = recv_packet_deser['device_id']
										sendData = recv_packet_ser
										uid = uuid.uuid4().bytes

										save_to_db(conn, uid, device_id, timestamp, sendData)

										pub_topic, _ = self.publishes_to[0]

										try:
												socket[pub_topic].send("{}!{}".format(
														pub_topic, json.dumps(sendData)), flags=zmq.NOBLOCK)

										except zmq.ZMQError:
												continue

								else:
										continue
						else:
								continue
