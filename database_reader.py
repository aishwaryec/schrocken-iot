import json
import sqlite3
import time
from datetime import datetime, timedelta


def read_from_db(conn, timestamp):

    query = '''SELECT data FROM telemetry WHERE datetime(timestamp) > datetime(?) '''
    c = conn.cursor()
    c.execute(query, (timestamp,))
    return c.fetchall()


class DBReader():

    def __init__(self, db):
        self.db = db

    def fetch_data(self, timestamp):

        rows = None
        conn = sqlite3.connect(
            self.db, detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES)
        conn.text_factory = str
        with conn as c:
            rows = read_from_db(c, timestamp)

        return rows
