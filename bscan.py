
import json
import time
from datetime import datetime
from multiprocessing import Lock, Process

import zmq
from bluepy.btle import DefaultDelegate, Scanner

SCAN_INTERVAL = 0.2


class DecodeErrorException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        """ if isNewDev:
            pass
        elif isNewData:
            pass """
        pass


class BaseScan(Process):

    def __init__(self, sensors, address):
        Process.__init__(self)
        self.sensors = sensors
        self.address = address


class Scanning(BaseScan):

    def run(self):

        topic, address = self.address
        ctx = zmq.Context()
        socket = ctx.socket(zmq.PUB)
        socket.bind(address)

        scanner = Scanner().withDelegate(ScanDelegate())

        while(True):

            try:

                devices = scanner.scan(SCAN_INTERVAL)
                ManuData = ""

                for dev in devices:

                    if dev.addr in self.sensors:

                        for (_, desc, value) in dev.getScanData():
                            if (desc == "16b Service Data"):
                                ManuData = value
                            else:
                                ManuData = ""

                        if (ManuData == ""):
                            continue

                        sendData = {}
                        sendData['timestamp'] = datetime.now().strftime(
                            '%Y-%m-%d %H:%M:%S.%f')
                        sendData['device_id'] = dev.addr
                        sendData['data'] = ManuData

                        try:
                            socket.send("{}!{}".format(
                                topic, json.dumps(sendData)), flags=zmq.NOBLOCK)

                        except zmq.ZMQError:
                            continue

                    else:
                        continue

            except DecodeErrorException:
                continue
