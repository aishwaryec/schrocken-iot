import json
import time
from multiprocessing import Lock, Process

import zmq
from mapping import *
import pprint

XTHRESH = 100
YTHRESH = 100
ZTHRESH = 100


def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0:  # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val                         # return positive value as is


class BaseDecode(Process):

    def __init__(self, sub_add, pub_add):
        Process.__init__(self)
        self.subscribes_to = sub_add
        self.publishes_to = pub_add


class Decoding(BaseDecode):

    def run(self):

        global XTHRESH
        global YTHRESH
        global ZTHRESH

        ctx = zmq.Context()
        socket = {}

        for line in self.subscribes_to:
            topic, addr = line
            socket[topic] = ctx.socket(zmq.SUB)
            socket[topic].connect(addr)
            socket[topic].setsockopt_string(
                zmq.SUBSCRIBE, unicode(topic))

        for line in self.publishes_to:
            topic, addr = line
            socket[topic] = ctx.socket(zmq.PUB)
            socket[topic].bind(addr)

        while(True):

            advDataHex = []

            for topic, addr in self.subscribes_to:
                advData = None
                try:
                    advData = socket[topic].recv(
                        flags=zmq.NOBLOCK)
                except zmq.ZMQError:
                    advData = None
                    continue

                if advData is not None:
                    # print(advData)
                    _, recv_packet_ser = advData.split('!')
                    recv_packet_deser = json.loads(recv_packet_ser)

                    timestamp = recv_packet_deser['timestamp']
                    device_id = recv_packet_deser['device_id']
                    manuData = recv_packet_deser['data']

                    for i, j in zip(manuData[::2], manuData[1::2]):
                        advDataHex.append(int(i+j, 16))

                    if((advDataHex[0] == 0xE1) and (advDataHex[1] == 0xFF)):
                        if(advDataHex[2] == 0xA1):

                            BatteryLevel = advDataHex[4]

                            acc_x_fixed = twos_comp(int("0x%0.2X%0.2X" % (
                                advDataHex[5], advDataHex[6]), 16), 16)
                            acc_y_fixed = twos_comp(int("0x%0.2X%0.2X" % (
                                advDataHex[7], advDataHex[8]), 16), 16)
                            acc_z_fixed = twos_comp(int("0x%0.2X%0.2X" % (
                                advDataHex[9], advDataHex[10]), 16), 16)

                            acc_x = (acc_x_fixed / 26.0)
                            acc_y = (acc_y_fixed / 26.0)
                            acc_z = (acc_z_fixed / 26.0)

                            if(acc_x > XTHRESH or acc_y > YTHRESH or acc_z > ZTHRESH):
                                continue
                            else:
                                sendData = {}
                                paramsData = {}
                                accData = {}

                                accData['x'] = acc_x
                                accData['y'] = acc_y
                                accData['z'] = acc_z

                                paramsData['battery'] = BatteryLevel
                                paramsData['acc'] = accData

                                sendData['timestamp'] = timestamp
                                sendData['device_id'] = device_id
                                sendData['params'] = paramsData

                                pp = pprint.PrettyPrinter(indent=4)
                                pp.pprint(sendData)

                                for topic, addr in self.subscribes_to:

                                    try:
                                        socket[topic].send("{}!{}".format(
                                            topic, json.dumps(sendData)), flags=zmq.NOBLOCK)

                                    except zmq.ZMQError:
                                        continue

                        else:
                            continue

                    else:
                        continue

                else:
                    continue
