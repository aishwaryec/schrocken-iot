
import socket
from random import randrange


def find_open_ports(num, minport, maxport):

    portcnt = num
    ports = []

    while(portcnt > 0):
        port = randrange(minport, maxport)

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.bind(('', port))
        except socket.error as e:
            continue

        portcnt = portcnt - 1
        ports.append(port)
        sock.close()

    return ports


def map_topics_to_ports(topics, ports):

    assert len(topics) == len(
        ports), "topics and ports need to have the same number of elements"
    assert len(topics) != 0, "length of the topics and ports list cannot be 0"

    return dict(zip(topics, ports))


def pub_addr_from_port(port):

    return "tcp://*:{}".format(port)


def sub_addr_from_port(port):

    return "tcp://127.0.0.1:{}".format(port)


def get_device_type_from_device_id(device_id):

    return


""" def get_topic_from_device_id(device_id):

    device_type = get_device_type_from_device_id(device_id)

    return get_topic_from_device_type(device_type) """
