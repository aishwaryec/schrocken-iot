import math


def get_pitch_and_roll(ax, ay, az):

    roll = math.atan2(ay, az) * (180.0/math.pi)
    pitch = math.atan2((-ax), math.sqrt((ay*ay + az*az))*(180.0/math.pi))
    return (roll, pitch)


def get_change_in_velocity(dt, prev_acc, inst_acc):

    return get_area_under_curve(dt, prev_acc, inst_acc)


def get_area_under_curve(dt, a, b):

    return (0.5 * dt * (a + b))

def detect_freefall():
    pass
