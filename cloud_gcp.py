import json
import time
from datetime import datetime, timedelta
from multiprocessing import Lock, Process

import database_reader
import zmq

LAST_TIMESTAMP = None
DB_READ_FLAG = False
HEARTBEAT = 60.0


def is_db_read_required(timestamp):
		global LAST_TIMESTAMP
		if LAST_TIMESTAMP is not None:
				dt = (datetime(timestamp) - datetime(LAST_TIMESTAMP)).total_seconds()
				return (dt >= HEARTBEAT)



def get_ts_and_data(row):

		_, _, timestamp, data = row

		return (timestamp, data)


class BaseHubManager(Process):

		def __init__(self, sub_add, db):
				Process.__init__(self)
				self.subscribes_to = sub_add
				self.db = db


class HubManager(BaseHubManager):

		def __init__(self, sub_add, db):
				BaseHubManager.__init__(self, sub_add, db)


		def run(self):
				global LAST_TIMESTAMP
				ctx = zmq.Context()
				socket = {}
				topic = None

				dbread = database_reader.DBReader(self.db)

				for line in self.subscribes_to:
						topic, addr = line
						socket[topic] = ctx.socket(zmq.SUB)
						socket[topic].connect(addr)
						socket[topic].setsockopt_string(
								zmq.SUBSCRIBE, unicode(topic))

				while(True):
						decodedData = None
						if DB_READ_FLAG:
								for row in dbread.fetch_data(LAST_TIMESTAMP):
										try:
												timestamp, sendData = get_ts_and_data(row)
												#send to gcp
										except Exception as e:
												continue

						else:
								try:
										decodedData = socket[topic].recv(flags=zmq.NOBLOCK)
								except zmq.ZMQError:
										decodedData = None


						if decodedData is not None:

								_, recv_packet_ser = decodedData.split('!')
								ser_new = recv_packet_ser.replace(
									"\\", "").replace("\"", "", 1)
								ser_rev = ser_new[::-1].replace("\"", "", 1)
								ser_fin = ser_rev[::-1]
								deser = json.loads(ser_fin)
								timestamp = deser['timestamp']

								print "{}\n".format(
									json.dumps(deser, indent=4, sort_keys=True))
								#send
						else:
								continue
