import json
import sys
import time
from multiprocessing import Lock, Process

import zmq
from adv_decode import BaseDecode, Decoding
from bscan import BaseScan, Scanning
from database_writer import DBWriter
from mapping import *
#from sensor import Sensor

WHITELISTED_SENSORS = ["ac:23:3f:26:0b:30", "ac:23:3f:26:0b:2f"]
#TOPICS = ["bleQ", "decodeQ", "sensorQ", "cloudQ"]
TOPICS = ["bleQ", "decodeQ"]
SCAN_INDEX = 0
DECODE_INDEX = 1
SENSOR_INDEX = 2
CLOUD_INDEX = 3
DATABASE = 'telemetry.db'
MIN_PORT = 30000
MAX_PORT = 60000


if __name__ == "__main__":

    open_ports = find_open_ports(len(TOPICS), MIN_PORT, MAX_PORT)

    top_port_map = map_topics_to_ports(TOPICS, open_ports)

    a = Scanning(WHITELISTED_SENSORS,
                 (TOPICS[SCAN_INDEX], pub_addr_from_port(top_port_map[TOPICS[SCAN_INDEX]])))
    b = Decoding([(TOPICS[SCAN_INDEX], sub_addr_from_port(top_port_map[TOPICS[SCAN_INDEX]]))],
                 [(TOPICS[DECODE_INDEX], pub_addr_from_port(top_port_map[TOPICS[DECODE_INDEX]]))])

    # c = Sensor([(TOPICS[DECODE_INDEX], sub_addr_from_port(top_port_map[TOPICS[DECODE_INDEX]]))],
    # 					 [(TOPICS[SENSOR_INDEX], pub_addr_from_port(top_port_map[TOPICS[SENSOR_INDEX]]))])

    # d = DBWriter(DATABASE, [(TOPICS[SENSOR_INDEX], sub_addr_from_port(top_port_map[TOPICS[SENSOR_INDEX]]))],
    # 						 [(TOPICS[CLOUD_INDEX], pub_addr_from_port(top_port_map[TOPICS[CLOUD_INDEX]]))])

    # e = HubManager([(TOPICS[CLOUD_INDEX], sub_addr_from_port(
    # 		top_port_map[TOPICS[CLOUD_INDEX]]))], DATABASE, CONNECTION_STRING)

    a.start()
    b.start()
    # c.start()
    # d.start()
    # e.start()

    a.join()
    b.join()
    # c.join()
    # d.join()
    # e.join()
